var url = require('url');  
var fs = require('fs');
var querystring = require('querystring');
var util = require('util');

var isEmpty = function(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true;
}

exports.get = function(req, res) {  
  req.requrl = url.parse(req.url, true);
  var path = req.requrl.pathname;
  if (/.(css)$/.test(path)) {
    res.writeHead(200, {
      'Content-Type': 'text/css'
    });
    fs.readFile(__dirname + path, 'utf8', function(err, data) {
      if (err) throw err;
      res.write(data, 'utf8');
      res.end();
    });
  } else {
    if (path === '/login') {
      login = require(('./controllers/signin'));
      var fullBody = '';
      req.on('data', function(chunk) {
        fullBody += chunk.toString();
      });
      req.on('end', function(){
        // usar datos
        res.writeHead(200, 'OK', {'Content-Type' : 'text/html'})
        var decodedBody = querystring.parse(fullBody);
        login.authenticated(function(u_hash) {
          if(!isEmpty(u_hash)) {
            // se trajo las credenciales
           global.logged_in = true;
          // console.log(util.inspect(u_hash['id']));
           global.uid = u_hash['id'];
           global.umail = u_hash['email'];
           console.log(util.inspect(u_hash));
           require('./controllers/home-mysql').get(req, res);
          } else {
            global.logged_in = false;
            login.get(req, res);
          }
        }, decodedBody['username'], decodedBody['user_pass']);
      });
      
    }
    else if (path === '/' || path === '/home') {
      if(global.logged_in) {
        require('./controllers/home-mysql').get(req, res);
      }
      else {
        require('./controllers/signin').get(req, res);
      }
      //
    } else if (path === '/logout') {
       global.logged_in = false;
       global.uid = -1;
       global.umail = '';
       //console.log('VALOR DE logged_in despues de logout ' + global.logged_in);
       require('./controllers/signin').get(req, res);
    } else if (path === '/comprar') {
       //console.log('VALOR DE logged_in es ' + global.logged_in);
       if(global.logged_in) {
        require('./controllers/compras').get(req, res);
      }
      else {
        require('./controllers/signin').get(req, res);
      }
    } else if(path === '/hacer_compra') {
      compras = require('./controllers/compras')
      req.on('data', function(chunk) {
        fullBody += chunk.toString();
      });
      req.on('end', function(){
        // usar datos
        res.writeHead(200, 'OK', {'Content-Type' : 'text/html'})
        var decodedBody = querystring.parse(fullBody);
        //console.log(util.inspect(decodedBody['undefinedselect_fruta']));
        compras.guardar_compras(function(r) {
          if(r) {
            // compro bien
           require('./controllers/home-mysql').get(req, res);
          } else {
            // mala compra
            if(global.logged_in) {
              compras.salio_mal(req, res);
            }
            else {
              require('./controllers/signin').get(req, res);
            }
          }
        }, decodedBody['cantidad_fruta'], decodedBody['codigo_fruta'], decodedBody['numero_tarjeta'], decodedBody['venc_tarjeta_year'], decodedBody['venc_tarjeta_mes'], decodedBody['venc_tarjeta_dia'], decodedBody['codigo_tarjeta'], decodedBody['undefinedfrutas_elegidas'], decodedBody['desc']);
      });
       
    }
    else {
      require('./controllers/404').get(req, res);
    }
  }
};

