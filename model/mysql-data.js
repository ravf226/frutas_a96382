var mysql = require('mysql'), 
util = require('util'),
dbconfig = require('../config/database') 
connection = mysql.createConnection(dbconfig.connection);
connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
});

var onErr = function(err, callback) {  
  connection.end();
  callback(err);
};

exports.ventas = function(callback) {
  connection.query('CALL seleccionar_ventas_de_usuario(?)', [global.uid], function(err, rows, fields) {
    if (err) onErr(err, callback);
    callback(false,rows);
  });
//connection.end();
};

exports.verificar_login = function(callback, username, password) {
  connection.query('CALL verificar_login(?, ?)', [username, password], function(err, rows, fields) {
    if (err) onErr(err, callback);
    callback(false,rows);
  });

};

exports.guardar_compras = function(callback, codigo, fecha, desc, usuario_id, fruta_id, monto, info_pago) {
  connection.query('CALL insertar_en_ventas(?, ?, ?, ?, ?, ?, ?)', [codigo, fecha, desc, usuario_id, fruta_id, monto, info_pago], function(err, rows, fields) {
    if (err) onErr(err, callback);
    callback(false,rows);
  });

};
