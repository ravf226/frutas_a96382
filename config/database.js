// config/database.js
module.exports = {
    'connection': {
        'host': process.env.FRUTAS_ADMIN_IP,
        'user': process.env.FRUTAS_ADMIN_USER,
        'password': process.env.FRUTAS_ADMIN_PASS,
        'database': process.env.FRUTAS_ADMIN_DATABASE
    }
};