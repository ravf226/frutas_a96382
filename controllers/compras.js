var template = require('../views/template-main');
var mysql_data = require('../model/mysql-data'); 
var util = require('util');
var emailjs = require('emailjs');
exports.get = function(req, res) {  
  mysql_data.ventas(function(err, ventas) {
    if (!err) {
      compras_form = '<form id="login_form" action="/hacer_compra" method="post"> <div class="frutas"> <div><select id="frutas_picker" name="frutas_elegidas"><option value="1|120.5" id="option_user">Aguacate| Valor: 120.5</option><option value="2|20" id="option_user">Fresas| Valor: 20</option><option value="3|200.3" id="option_user">Chayote| Valor: 200.3</option><option value="4|20.2" id="option_user">Banano| Valor: 20.2</option><option value="5|45" id="option_user">Platano| Valor: 45</option><option value="6|350.25" id="option_user">Kiwis| Valor: 350.25</option></select> </div> <br/> <div> <input type="number" min="1" name="cantidad_fruta" id="cantidad_fruta"/> <label for="cantidad_fruta">Digite la cantidad de fruta.</label> </div><br/> <div> <input type="text" value="" placeholder="Codigo de Compra" name="codigo_fruta" id="codigo_fruta"/> <label for="codigo_fruta">Digite el codigo de la compra.</label> </div> <br/> <div> <input type="text" value="" placeholder="Desc de Compra" name="desc" id="desc"/> <label for="desc">Digite la desc de la compra.</label> </div> <br/> <div> <input type="text" value="" placeholder="ejm, 11257895123" name="numero_tarjeta" id="numero_tarjeta"/> <label for="numero_tarjeta">Digite # de tarjeta.</label> </div> <br/> <p>Digite la fecha de vencimiento de la tarjeta</p> <div> <input type="numeric" value="" name="venc_tarjeta_dia" id="venc_tarjeta_dia" placeholder="23"/> <label for="venc_tarjeta_dia">DD</label> </div> <div> <input type="numeric" value="" name="venc_tarjeta_mes" id="venc_tarjeta_mes" placeholder="07"/> <label for="venc_tarjeta_mes">MM</label> </div> <div> <input type="numeric" value="" name="venc_tarjeta_year" id="venc_tarjeta_year" placeholder="2018"/> <label for="venc_tarjeta_year">YYYY</label> </div> <br/> <div> <input type="password" value="" placeholder="ejm, 718" name="codigo_tarjeta" id="codigo_tarjeta"/> <label for="codigo_tarjeta">Digite Codigo de seguridad de Tarjeta.</label> </div> <button type="submit">Comprar</button></div></form>';
      res.writeHead(200, {
        'Content-Type': 'text/html'
      });
      res.write(template.build("Frutas_a96382", "Frutas A96382", compras_form));
      res.end();
    } else {
      res.writeHead(200, {
        'Content-Type': 'text/html'
      });
      res.write(template.build("Oh dear", "Error de Base de datos", "<p>Error details: " + err + "</p>"));
      res.end();
    }
  });
};

exports.salio_mal = function(req, res) {
  mysql_data.ventas(function(err, ventas) {
    if (!err) {
      compras_form = '<h3>Algo salio mal en su compra. Intentelo de nuevo.</h3><form id="login_form" action="/hacer_compra" method="post"> <div class="frutas"> <div><select id="frutas_picker" name="frutas_elegidas"><option value="1|120.5" id="option_user">Aguacate| Valor: 120.5</option><option value="2|20" id="option_user">Fresas| Valor: 20</option><option value="3|200.3" id="option_user">Chayote| Valor: 200.3</option><option value="4|20.2" id="option_user">Banano| Valor: 20.2</option><option value="5|45" id="option_user">Platano| Valor: 45</option><option value="6|350.25" id="option_user">Kiwis| Valor: 350.25</option></select> </div> <br/> <div> <input type="number" min="1" name="cantidad_fruta" id="cantidad_fruta"/> <label for="cantidad_fruta">Digite la cantidad de fruta.</label> </div><br/> <div> <input type="text" value="" placeholder="Codigo de Compra" name="codigo_fruta" id="codigo_fruta"/> <label for="codigo_fruta">Digite el codigo de la compra.</label> </div> <br/> <div> <input type="text" value="" placeholder="Desc de Compra" name="desc" id="desc"/> <label for="desc">Digite la desc de la compra.</label> </div> <br/> <div> <input type="text" value="" placeholder="ejm, 11257895123" name="numero_tarjeta" id="numero_tarjeta"/> <label for="numero_tarjeta">Digite # de tarjeta.</label> </div> <br/> <p>Digite la fecha de vencimiento de la tarjeta</p> <div> <input type="numeric" value="" name="venc_tarjeta_dia" id="venc_tarjeta_dia" placeholder="23"/> <label for="venc_tarjeta_dia">DD</label> </div> <div> <input type="numeric" value="" name="venc_tarjeta_mes" id="venc_tarjeta_mes" placeholder="07"/> <label for="venc_tarjeta_mes">MM</label> </div> <div> <input type="numeric" value="" name="venc_tarjeta_year" id="venc_tarjeta_year" placeholder="2018"/> <label for="venc_tarjeta_year">YYYY</label> </div> <br/> <div> <input type="password" value="" placeholder="ejm, 718" name="codigo_tarjeta" id="codigo_tarjeta"/> <label for="codigo_tarjeta">Digite Codigo de seguridad de Tarjeta.</label> </div> <button type="submit">Comprar</button></div></form>';
      res.writeHead(200, {
        'Content-Type': 'text/html'
      });
      res.write(template.build("Frutas_a96382", "Frutas A96382", compras_form));
      res.end();
    } else {
      res.writeHead(200, {
        'Content-Type': 'text/html'
      });
      res.write(template.build("Oh dear", "Error de Base de datos", "<p>Error details: " + err + "</p>"));
      res.end();
    }
  });
};


exports.guardar_compras = function(callback, cantidad_fruta, codigo_fruta, numero_tarjeta, venc_tarjeta_year, venc_tarjeta_mes, venc_tarjeta_dia, codigo_tarjeta, select_fruta, desc) {
  //console.log(util.inspect(select_fruta));
  try {
    var fruta_id = parseInt(select_fruta.split('|')[0]);
    var monto = parseFloat(select_fruta.split('|')[1]) * parseFloat(cantidad_fruta);
    var t = new Date().toISOString().slice(0, 19).replace('T', ' ');
    var info = numero_tarjeta + '|' + venc_tarjeta_year + '-' + venc_tarjeta_mes + '-'  + venc_tarjeta_dia + '|' + codigo_tarjeta;
    mysql_data.guardar_compras(function(err, usuario) {
    result = false;
      if(!err) {
        result = true;
        // enviamos correo
        var server = emailjs.server.connect({
         user:'rvalenciano226@yahoo.com',
         password: 'Bizancio2001',
         host: 'smtp.mail.yahoo.com',
         tls: true,
         port: 587
      });
    
      // send the message and get a callback with an error or details of the message that was sent
      server.send({
        text:    "Ha comprado una fruta",     
        from:    "rvalenciano226@yahoo.com", 
        to:      global.umail,
        subject: "Frutas A96382 Le informa:"
      }, function(err, message) { console.log(err || message); });
      
      }
      callback(result);
    }, codigo_fruta, t, desc, global.uid, fruta_id, monto, info);
  }
  catch (e) {
    result = false;
    callback(result);
  }
};
