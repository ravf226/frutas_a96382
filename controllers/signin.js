var template = require('../views/template-main');
var mysql_data = require('../model/mysql-data'); 
var util = require('util');   
exports.get = function(req, res) {  
  mysql_data.ventas(function(err, ventas) {
    if (!err) {
      login_form = '<form id="login" method="POST" action="/login"><input id="username" type="text" name="username" placeholder="Usuario"/><label for="username">Digite el nombre de usuario</label></br></br><input type="password" value="" placeholder="Password" name="user_pass" id="login_pass"/><label for="user_pass">Digite el Password</label></br></br><input type="submit" value="Entrar"/></form>';
      res.writeHead(200, {
        'Content-Type': 'text/html'
      });
      res.write(template.build("Frutas_a96382", "Frutas A96382", login_form));
      res.end();
    } else {
      res.writeHead(200, {
        'Content-Type': 'text/html'
      });
      res.write(template.build("Oh dear", "Error de Base de datos", "<p>Error details: " + err + "</p>"));
      res.end();
    }
  });
};

exports.authenticated = function(callback, username, pass) {
  mysql_data.verificar_login(function(err, usuario) {
    result = {};
    if(!err) {
      if(usuario[0][0]['COUNT(*)'] > 0) {
        result = usuario[0][0];
      }
      else {
        result = {}
      }
    }
    callback(result);
  }, username, pass);
};
