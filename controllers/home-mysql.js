var template = require('../views/template-main');  
var mysql_data = require('../model/mysql-data'); 
var util = require('util');
exports.get = function(req, res) {  
  mysql_data.ventas(function(err, ventas) {
    if (!err) {
      var lista_ventas = "",
        i = 0;
      for (i = 0; i < ventas[0].length; i++) {

        lista_ventas += "<tr><td>" + ventas[0][i].codigo + "</td>";
        lista_ventas += "<td>" + ventas[0][i].nombre + "</td>";
        lista_ventas += "<td>" + ventas[0][i].descripcion + "</td>";
        lista_ventas += "<td>" + ventas[0][i].monto + "</td>";
        lista_ventas += "</tr>";
      }

      var tabla_ventas = "<table><thead><th>Codigo</th>";
      tabla_ventas += "<th>Fruta</th>";
      tabla_ventas += "<th>Descripcion</th>"; 
      tabla_ventas += "<th>Monto</th>"; 
      tabla_ventas += "</thead><tbody>" + lista_ventas + "</tbody>"; 
      tabla_ventas += "</table>";
      

      res.writeHead(200, {
        'Content-Type': 'text/html'
      });
      res.write(template.build("Compras Realizadas", "Hola", "<ul><li><a href='http://localhost:8899/comprar'>Comprar frutas</a></li><li><a href='http://localhost:8899/home'>Ver Compras</a></li><li><a href='http://localhost:8899/logout'>Logout</a></li></ul><p>Se le han vendido las siguientes frutas</p>" + tabla_ventas));
      res.end();
    } else {
      res.writeHead(200, {
        'Content-Type': 'text/html'
      });
      res.write(template.build("Oh dear", "Database error", "<p>Error details: " + err + "</p>"));
      res.end();
    }
  });
};
