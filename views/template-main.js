exports.build = function(title, pagetitle, content) {  
  return ['<!doctype html>',
  '<html lang="en"><meta charset="utf-8">\n<title>{title}</title>',
  '<link rel="stylesheet" href="/assets/style.css" />',
  '<h1>{pagetitle}</h1>\n',
  '<div id="content">{content}</div>',
  '<p class="copy">&copy; Copyright 2015<span>|</span>Randall Valenciano Fallas<span>|</span>Universidad de Costa Rica</p><div class="cl">&nbsp;</div>',
  '<p>Olvido su password? Recuperelo <a href="http://192.168.122.2:7080/~usuario13/cgi-bin/digiteCorreo.cgi">aqui</a>']
  .join('\n')
  .replace(/{title}/g, title)
  .replace(/{pagetitle}/g, pagetitle)
  .replace(/{content}/g, content);
};
